" kytdkut's init.vim

" vim-plug & plugin setup
"

call plug#begin('~/.config/nvim/plugged')

" colorscheme
"Plug 'connorholyday/vim-snazzy'
Plug 'dracula/vim'
"Plug 'challenger-deep-theme/vim', { 'as': 'challenger-deep' }

" autocompletion
Plug 'Shougo/deoplete.nvim'
Plug 'deoplete-plugins/deoplete-zsh', { 'for': 'zsh' }
Plug 'deoplete-plugins/deoplete-jedi', { 'for': 'python' }
Plug 'ervandew/supertab'

" language packs
Plug 'sheerun/vim-polyglot' " collection of language packs
" Plug 'stevearc/vim-arduino', { 'for': 'arduino' }
Plug 'sophacles/vim-processing', { 'for': 'processing' }
Plug 'supercollider/scvim', { 'for': 'supercollider' }

" IDE features (just python for now)
Plug 'dense-analysis/ale', { 'for': 'python' } " Asynchronous Lint Engine, currently using flake8, yapf (see bottom)
Plug 'davidhalter/jedi-vim', { 'for': 'python' } "documentation, completion, goto assignments/definitions, renaming, usage

" authoring markdown
Plug 'dhruvasagar/vim-table-mode', { 'for': 'markdown' }
Plug 'mzlogin/vim-markdown-toc', { 'for': 'markdown' } "table of contents generator
Plug 'junegunn/goyo.vim', { 'for': 'markdown,text' } "distraction-free writing

" misc
Plug 'itchyny/lightline.vim'
"Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround'
Plug 'lambdalisue/suda.vim'
Plug 'vitalk/vim-simple-todo'
Plug 'vim-scripts/restore_view.vim' "restoring cursor position & fold status
Plug 'francoiscabrol/ranger.vim', { 'on': 'Ranger' } "ranger integration
Plug 'rbgrouleff/bclose.vim', { 'on': 'Ranger' } "ranger.vim dependency
Plug 'junegunn/vim-peekaboo' "peek on registers
Plug 'tommcdo/vim-exchange' "swap text regions
Plug 'thinca/vim-visualstar' "improved star

call plug#end()

" options
"

filetype plugin indent on
syntax on

set tgc " disable for pywal
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set tabstop=4
set softtabstop=0
set shiftwidth=4
set copyindent
set preserveindent
set smartindent
set nowrap
set mouse=a
let mapleader="`"
set omnifunc=syntaxcomplete#Complete
set number
set nocursorline
set ignorecase
set smartcase
set splitbelow
set viewoptions=cursor,folds,slash,unix " needed by restore_view plugin
set nofoldenable
set wildignore+=*.meta,*.bak,*.old
set noswapfile
set noshowmode
if has("unix")
  let s:uname = system("uname -s")
  if s:uname == "Linux"
	let g:python3_host_prog = '/usr/bin/python3'
  endif
endif

" theme
"
"if filereadable(expand("~/.vimrc_background"))
"	source ~/.vimrc_background
"endif

colorscheme dracula
let g:lightline = {
\ 'colorscheme': 'dracula',
\ }

hi Normal ctermbg=NONE guibg=NONE
hi NonText ctermbg=NONE guibg=NONE
hi NonText ctermfg=NONE guifg=NONE

"hi MatchParen ctermbg=0 ctermfg=8
"hi cursorLine cterm=NONE
"hi TabLine ctermbg=NONE ctermfg=8 cterm=NONE
"hi Pmenu ctermbg=8 ctermfg=0
"hi PmenuSel ctermbg=4 ctermfg=0

" mappings
"

nnoremap <S-q> <Esc>:exit<CR>
nnoremap <silent> <C-s> :update<CR>
inoremap <silent> <C-s> <Esc>:update<CR>
vnoremap <silent> <C-s> :update<CR>
command SudoWrite :w suda://%

" movement between windows
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" de highlight current search
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

" copy to clipboard
vnoremap <leader>y  "+y
nnoremap <leader>Y  "+yg_
nnoremap <leader>y  "+y
nnoremap <leader>yy  "+yy

" paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" indent whole file
nnoremap <silent> <Leader>= migg=G`i

" plugin & language options
"
"

"let loaded_matchparen = 1

" markdown
let g:table_mode_corner='|'
let g:mkdp_refresh_slow = 1

augroup filetype_md
	au!
	" sheerun/vim-polyglot: plasticboy/vim-markdown
	au FileType markdown let g:vim_markdown_override_foldtext = 0
	au FileType markdown let g:vim_markdown_no_default_key_mappings = 1
	au FileType markdown let g:vim_markdown_math = 1
	au FileType markdown set wrap lbr
	"au FileType markdown setlocal spell spelllang=es,en_us
	au FileType markdown nnoremap j gj
	au FileType markdown nnoremap k gk
	au FileType markdown nnoremap <silent> <C-B> ciw**<C-R>"**<ESC>
	au FileType markdown nnoremap <silent> <C-I> ciw*<C-R>"*<ESC>
	au FileType markdown vnoremap <silent> <C-B> c**<C-R>"**<ESC>
	au FileType markdown vnoremap <silent> <C-I> c*<C-R>"*<ESC>
augroup END

" txt
augroup filetype_text
	au!
	au FileType text nnoremap j gj
	au FileType text nnoremap k gk
	au FileType text set wrap lbr
augroup END

" deoplete
let g:deoplete#enable_at_startup = 1

" supertab
let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabContextDefaultCompletionType = "<c-x><c-o>"
let g:SuperTabDefaultCompletionTypeDiscovery = ["&omnifunc:<c-x><c-o>","&completefunc:<c-x><c-n>"]
let g:SuperTabClosePreviewOnPopupClose = 1

" supercollider
au BufEnter,BufWinEnter,BufNewFile,BufRead *.sc,*.scd set filetype=supercollider

let g:sclangTerm = "alacritty -e"
let g:sclangPipeApp = "~/.config/nvim/plugged/scvim/bin/start_pipe"
let g:sclangDispatcher = "~/.config/nvim/plugged/scvim/bin/sc_dispatcher"
let g:scFlash = 1

augroup filetype_supercollider
	au!
	au FileType supercollider nnoremap <buffer> <leader>sp :SClangKill<CR>
	au FileType supercollider nnoremap <buffer> <leader>ss :SClangStart<CR>
	au FileType supercollider nnoremap <buffer> <leader>x :call SClangHardstop()<CR>
	au Filetype supercollider nnoremap <buffer> <leader>b :call SClang_block()<CR>
	au Filetype supercollider nnoremap <buffer> <leader>l :call SClang_send()<CR>
	au Filetype supercollider nnoremap <buffer> <C-c> :call SClangHardstop()<CR>
	au Filetype supercollider set nocindent
augroup END

" ranger
let g:ranger_replace_netrw = 1
let g:ranger_map_keys = 0
nnoremap <silent> - :Ranger<CR>

" arduino
" let g:arduino_cmd = '/usr/bin/arduino'
" let g:arduino_dir = '/usr/share/arduino'
" let g:arduino_auto_baud = 1
" let g:arduino_serial_port = '/dev/ttyUSB0'
" let g:arduino_board = 'archlinux-arduino:avr:uno'
" augroup filetype_arduino
" 	au!
" 	au FileType arduino nnoremap <buffer> <leader>v :ArduinoVerify<CR>
" 	au FileType arduino nnoremap <buffer> <leader>u :ArduinoUpload<CR>
" 	au FileType arduino nnoremap <buffer> <leader>d :ArduinoUploadAndSerial<CR>
" 	au FileType arduino nnoremap <buffer> <leader>b :ArduinoChooseBoard<CR>
" 	au FileType arduino nnoremap <buffer> <leader>p :ArduinoChooseProgrammer<CR>
" augroup END

" python
autocmd FileType python setlocal completeopt-=preview

let g:jedi#goto_command = "<leader>d"
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_definitions_command = "<F2>"
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#rename_command = "<leader>r"
"let g:jedi#completions_command = "<C-Space>"
let g:jedi#completions_enabled = 0 " using deoplete-jedi

" ale
let g:ale_linters = {
			\ 'python': ['flake8'],
			\}
let g:ale_fixers = ['yapf']
let g:ale_sign_column_always = 1
