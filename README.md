# dotfiles
my dotfiles

I'm currently using:

- arch linux
- bspwm
- polybar
- jgmenu
- alacritty
- neovim
- source sans pro & source code pro (nerd-patched)

config files for alacritty, polybar, dunst and jgmenu are symlinks to ~/.cache/wal/{alacritty,polybar,dunst,jgmenu}
