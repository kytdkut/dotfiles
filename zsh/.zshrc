# .zshrc
#

export HISTFILE=~/.zshhistfile
export HISTSIZE=100
export SAVEHIST=1000

zmodload zsh/zpty # for deoplete-zsh (neovim)

# sources
#

if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
	. "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi
. ~/.zshaliases
. ~/.zshenv
. ~/.zshfunc
. /usr/share/fzf/key-bindings.zsh

# options
# 
unsetopt beep

# keybindings
bindkey "^[[3~" delete-char
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[5~" beginning-of-history
bindkey "^[[6~" end-of-history
bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word

bindkey -s "^E" 'nvim "/home/mauro/Stuff/Markdown repos/Notes/Inbox.md"'
bindkey -s "" "ranger"
