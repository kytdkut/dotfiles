emulate sh -c 'source /etc/profile'

[[ -f ~/.zsh/env ]] && . ~/.zsh/env

if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
  exec startx -- vt1 &> /dev/null
fi
